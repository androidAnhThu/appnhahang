package com.example.quanlynhahang;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.quanlynhahang.adapter.MonDaChonAdapter;
import com.example.quanlynhahang.dao.ChiTietBanDAO;
import com.example.quanlynhahang.dao.MonAnDao;
import com.example.quanlynhahang.model.ChiTietBan;
import com.example.quanlynhahang.model.MonAn;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class MonDaChonFragment extends Fragment {
    String maBan;
    String maMonAn;


    RecyclerView rv_mondachon;
    Context c;

    ChiTietBan chiTietBan;
    ChiTietBanDAO chiTietBanDAO;
    MonDaChonAdapter monDaChonAdapter;
    List<ChiTietBan> list;


    public MonDaChonFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mon_da_chon, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rv_mondachon=view.findViewById(R.id.rv_mondachon);
        rv_mondachon.setHasFixedSize(true);
        rv_mondachon.setLayoutManager(new LinearLayoutManager(c));

        //lấy mã bàn
        Intent intent=getActivity().getIntent();
        Bundle bundle=intent.getBundleExtra("ban");
        if(bundle!=null) {
            maBan=bundle.getString("maBan");
        }

        DatabaseReference mDatabase= FirebaseDatabase.getInstance().getReference("ChiTietBan");
        list=new ArrayList<>();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if(data.child("maBan").getValue(String.class).equals(maBan)) {
                        ChiTietBan chiTietBan = data.getValue(ChiTietBan.class);
                        list.add(chiTietBan);

                        monDaChonAdapter=new MonDaChonAdapter(getContext(),list);
                        rv_mondachon.setAdapter(monDaChonAdapter);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
    }

    public void capNhatLV(){
        monDaChonAdapter.notifyItemInserted(list.size());
        monDaChonAdapter.notifyDataSetChanged();
    }

    public void xoaMonDaChon(ChiTietBan chiTietBan){
        chiTietBanDAO.delete(chiTietBan);
        capNhatLV();
    }

}
