package com.example.quanlynhahang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.quanlynhahang.adapter.BanAdapter;
import com.example.quanlynhahang.adapter.MonAnAdapter2;
import com.example.quanlynhahang.adapter.SectionsPagerAdapter;
import com.example.quanlynhahang.dao.BanDAO;
import com.example.quanlynhahang.dao.ChiTietBanDAO;
import com.example.quanlynhahang.dao.HoaDonBanChiTietDAO;
import com.example.quanlynhahang.dao.HoaDonBanDAO;
import com.example.quanlynhahang.dao.MonAnDao;
import com.example.quanlynhahang.model.Ban;
import com.example.quanlynhahang.model.ChiTietBan;
import com.example.quanlynhahang.model.HoaDonBan;
import com.example.quanlynhahang.model.HoaDonBanChiTiet;
import com.example.quanlynhahang.model.MonAn;
import com.example.quanlynhahang.model.NhanVien;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ChonMonAnActivity extends AppCompatActivity {
    String maBan;
    String maMonAn;

    ViewPager viewPager;
    TabLayout tabLayout;
    SectionsPagerAdapter sectionsPagerAdapter;

    ChiTietBan chiTietBan;

    static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chon_mon_an);


        viewPager = findViewById(R.id.viewpager);
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        sectionsPagerAdapter.addFragment(new KhaiViFragment());
        sectionsPagerAdapter.addFragment(new MonChinhFragment());
        sectionsPagerAdapter.addFragment(new TrangMiengFragment());
        sectionsPagerAdapter.addFragment(new NuocUongFragment());
        viewPager.setAdapter(sectionsPagerAdapter);

        //tab layout
        tabLayout=findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setText("Khai vị");
        tabLayout.getTabAt(1).setText("Món chính");
        tabLayout.getTabAt(2).setText("Tráng miệng");
        tabLayout.getTabAt(3).setText("Nước uống");
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        loadMonDaChonFragment();


        //lấy mã bàn
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("ban");
        if(bundle!=null) {
            maBan = bundle.getString("maBan");
            setTitle("Bạn đang chọn bàn " + maBan);
        }

        //lấy mã món ăn
        Intent intent2=getIntent();
        Bundle bundle2=intent2.getBundleExtra("mon_an");
        if(bundle2!=null) {
            maMonAn = bundle2.getString("ma_mon_an");
        }
    }

    public void loadMonDaChonFragment(){
        FragmentManager fragmentManager=getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.frame_layout,new MonDaChonFragment()).commit();

    }

    public String layMaBan(){
        //lấy mã bàn
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("ban");
        if(bundle!=null) {
            maBan=bundle.getString("maBan");
            setTitle("Bạn đang chọn bàn "+maBan);
        }
        return maBan;
    }

    public void layMaMonAn(){
        //lấy mã món ăn
        Intent intent2=getIntent();
        Bundle bundle2=intent2.getBundleExtra("monan");
        if(bundle2!=null) {
            maMonAn=bundle2.getString("mamonan");
        }
    }

    public void themMonDaChon(){
        chiTietBan=new ChiTietBan();
        DatabaseReference mDatabase= FirebaseDatabase.getInstance().getReference("MonAn");
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if(data.child("maMonAn").getValue(String.class).equals(maMonAn)) {
                        MonAn monAn = data.getValue(MonAn.class);

                        chiTietBan.setMonAn(monAn);
                        chiTietBan.setMaBan("T101");
                        chiTietBan.setSoLuong(1);
                        ChiTietBanDAO chiTietBanDAO=new ChiTietBanDAO(ChonMonAnActivity.this);
                        chiTietBanDAO.insert(chiTietBan);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.chon_mon_an_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_thanhtoan:




                //hóa đơn
                final HoaDonBan hoaDonBan=new HoaDonBan();
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                final String email=user.getEmail();
                DatabaseReference mDatabase= FirebaseDatabase.getInstance().getReference("NhanVien");
                ValueEventListener postListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot data : dataSnapshot.getChildren()) {
                            if(data.child("email").getValue(String.class).equals(email)) {
                                NhanVien nhanVien = data.getValue(NhanVien.class);
                                hoaDonBan.setNhanVien(nhanVien);
                                hoaDonBan.setNhanVien(nhanVien);

                                Calendar c = Calendar.getInstance();
                                String strDate = sdf.format(c.getTime());
                                hoaDonBan.setThoiGian(strDate);

                                hoaDonBan.setMaBan(maBan);

                                HoaDonBanDAO hoaDonBanDAO=new HoaDonBanDAO(ChonMonAnActivity.this);
                                hoaDonBanDAO.insert(hoaDonBan);

                                huyBan(maBan);
                                huyChiTietBan(maBan);

                                Intent intent=new Intent(ChonMonAnActivity.this,ThanhToanActivity.class);
                                intent.putExtra("ma_ban",maBan);
                                startActivity(intent);

                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                    }
                };
                mDatabase.addValueEventListener(postListener);


                //hóa đơn chi tiết
                HoaDonBanChiTiet hoaDonBanChiTiet=new HoaDonBanChiTiet();
                hoaDonBanChiTiet.setHoaDonBan(hoaDonBan);
                hoaDonBanChiTiet.setMonAn(chiTietBan.getMonAn());
                hoaDonBanChiTiet.setSoLuong(chiTietBan.getSoLuong());
                HoaDonBanChiTietDAO hoaDonBanChiTietDAO=new HoaDonBanChiTietDAO(ChonMonAnActivity.this);
                hoaDonBanChiTietDAO.insert(hoaDonBanChiTiet);



                return true;
            case R.id.menu_inhoadon:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void huyBan(final String maBan){
        final DatabaseReference mDatabase= FirebaseDatabase.getInstance().getReference("Ban");
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child("maBan").getValue(String.class).equals(maBan)) {
                        mDatabase.child(data.getKey()).child("trangThai").setValue("trong")
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(ChonMonAnActivity.this, "Sửa thành công", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(ChonMonAnActivity.this, "Sửa thất bại", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void huyChiTietBan(final String maBan){
        final DatabaseReference mDatabase= FirebaseDatabase.getInstance().getReference("ChiTietBan");
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child("maBan").getValue(String.class).equals(maBan)) {
                        mDatabase.child(data.getKey()).setValue(null)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(ChonMonAnActivity.this, "Sửa thành công", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(ChonMonAnActivity.this, "Sửa thất bại", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}
