package com.example.quanlynhahang;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.quanlynhahang.dao.LoaiMonAnDAO;
import com.example.quanlynhahang.model.LoaiMonAn;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class LoaiMonAnActivity extends AppCompatActivity {
    ListView lvLoaiMonAn;
    FloatingActionButton fl_them;

    List<LoaiMonAn> list;
    LoaiMonAnDAO loaiMonAnDAO;
    LoaiMonAn loaiMonAn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loai_mon_an);

        lvLoaiMonAn=findViewById(R.id.lvLoaiMonAn);
        fl_them=findViewById(R.id.fl_them);

        showDSLMA();
    }

    public void showDSLMA(){
        list=new ArrayList<>();
        loaiMonAnDAO=new LoaiMonAnDAO(LoaiMonAnActivity.this);
        list=loaiMonAnDAO.getAll();
        ArrayAdapter<LoaiMonAn> adapter=new ArrayAdapter<LoaiMonAn>(this,android.R.layout.simple_list_item_1,list);
        lvLoaiMonAn.setAdapter(adapter);
    }
}
