package com.example.quanlynhahang.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.quanlynhahang.ChiTietGoiMonActivity;
import com.example.quanlynhahang.ChonMonAnActivity;
import com.example.quanlynhahang.DanhSachBanActivity;
import com.example.quanlynhahang.R;
import com.example.quanlynhahang.dao.BanDAO;
import com.example.quanlynhahang.model.Ban;
import com.example.quanlynhahang.model.ChiTietBan;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class BanAdapter2 extends BaseAdapter {
    List<Ban> list;
    Context context;
    public LayoutInflater inflater;
    BanAdapter2.ViewHolder holder;

    public BanAdapter2(List<Ban> list, Context context) {
        super();
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Ban ban = list.get(position);
        if (convertView == null) {
            holder = new BanAdapter2.ViewHolder();
            convertView = inflater.inflate(R.layout.one_cell_ban_2, null);

            //ánh xạ
            holder.ivAnh= (ImageView) convertView.findViewById(R.id.imgAnh);
            holder.tvMaBan= (TextView) convertView.findViewById(R.id.tvMaBan);
            holder.ivMore= (ImageView) convertView.findViewById(R.id.imgMore);


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, ChonMonAnActivity.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("maBan",ban.getmaBan());
                    intent.putExtra("ban",bundle);
                    context.startActivity(intent);


                    ban.setTrangThai("dachon");

                    BanDAO banDAO=new BanDAO(context);
                    banDAO.update(ban);
                    list.clear();
                    list.addAll(banDAO.getAll2());
                    BanAdapter2.this.notifyDataSetChanged();
                }
            });

            holder.ivMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    PopupMenu popup = new PopupMenu(context, v);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.menu_ban, popup.getMenu());
                    popup.show();

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.menu_thanhtoan:

                                    return true;
                                case R.id.menu_huyban:
                                    ban.setTrangThai("trong");
                                    BanDAO banDAO=new BanDAO(context);
                                    banDAO.update(ban);
                                    list.clear();
                                    BanAdapter2.this.notifyDataSetChanged();

                                    xoaBanTheoMaBan(ban.getmaBan());
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                }
            });

            //set data lên layout custom
            holder.tvMaBan.setText(ban.getmaBan());
            if(ban.getTrangThai().equals("dachon")){
                holder.ivAnh.setImageResource(R.drawable.table_chon);
            }
            else
                holder.ivAnh.setImageResource(R.drawable.table);

            convertView.setTag(holder);
        } else
            holder = (BanAdapter2.ViewHolder) convertView.getTag();
        return convertView;
    }

    public static class ViewHolder {
        ImageView ivAnh,ivMore;
        TextView tvMaBan;
    }

    public void xoaBanTheoMaBan(final String maBan){
        final DatabaseReference mDatabase= FirebaseDatabase.getInstance().getReference("ChiTietBan");
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child("maBan").getValue(String.class).equals(maBan)){
                        mDatabase.child(data.getKey()).removeValue()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(context, "Xóa thành công", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(context, "Xóa thất bại", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}