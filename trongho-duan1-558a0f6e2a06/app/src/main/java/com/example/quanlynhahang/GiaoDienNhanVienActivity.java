package com.example.quanlynhahang;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GiaoDienNhanVienActivity extends AppCompatActivity {
    Button btnDanhSachBan,btnNhapNguyenLieu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_giao_dien_nhan_vien);

        btnDanhSachBan=findViewById(R.id.btnDanhSachBan);
        btnNhapNguyenLieu=findViewById(R.id.btnNhapNguyenLieu);

        danhSachBan();
        nhapNguyenLieu();
    }

    public void nhapNguyenLieu(){
        btnNhapNguyenLieu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GiaoDienNhanVienActivity.this,NhapNguyenLieuActivity.class);
                startActivity(intent);
            }
        });
    }

    public void danhSachBan(){
        btnDanhSachBan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GiaoDienNhanVienActivity.this,DanhSachBanActivity.class);
                startActivity(intent);
            }
        });
    }
}
