package com.example.quanlynhahang.model;

public class NguyenLieu {
    String maNguyenLieu,tenNguyenLieu;

    public NguyenLieu() {
        //mặc định của Firebase khi nhận data
    }

    public NguyenLieu(String maNguyenLieu, String tenNguyenLieu) {
        this.maNguyenLieu = maNguyenLieu;
        this.tenNguyenLieu = tenNguyenLieu;
    }

    public String getMaNguyenLieu() {
        return maNguyenLieu;
    }

    public void setMaNguyenLieu(String maNguyenLieu) {
        this.maNguyenLieu = maNguyenLieu;
    }

    public String getTenNguyenLieu() {
        return tenNguyenLieu;
    }

    public void setTenNguyenLieu(String tenNguyenLieu) {
        this.tenNguyenLieu = tenNguyenLieu;
    }
}
