package com.example.quanlynhahang.model;

public class ChiTietBan {
    String maChiTietBan;
    String maBan;
    MonAn monAn;
    int soLuong;

    public ChiTietBan(String maChiTietBan,String maBan, MonAn monAn, int soLuong) {
        this.maChiTietBan=maChiTietBan;
        this.maBan = maBan;
        this.monAn=monAn;
        this.soLuong = soLuong;
    }

    public ChiTietBan() {
    }

    public String getMaChiTietBan() {
        return maChiTietBan;
    }

    public void setMaChiTietBan(String maChiTietBan) {
        this.maChiTietBan = maChiTietBan;
    }

    public String getMaBan() {
        return maBan;
    }

    public void setMaBan(String maBan) {
        this.maBan = maBan;
    }

    public MonAn getMonAn() {
        return monAn;
    }

    public void setMonAn(MonAn monAn) {
        this.monAn = monAn;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }
}
