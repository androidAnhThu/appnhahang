package com.example.quanlynhahang.model;

public class HoaDonBanChiTiet {
    String maHDCT;
    HoaDonBan hoaDonBan;
    MonAn monAn;
    int soLuong;

    public HoaDonBanChiTiet(String maHDCT,HoaDonBan hoaDonBan, MonAn monAn, int soLuong) {
        this.maHDCT=maHDCT;
        this.hoaDonBan=hoaDonBan;
        this.monAn=monAn;
        this.soLuong = soLuong;
    }

    public HoaDonBanChiTiet() {
    }

    public String getMaHDCT() {
        return maHDCT;
    }

    public void setMaHDCT(String maHDCT) {
        this.maHDCT = maHDCT;
    }

    public HoaDonBan getHoaDonBan() {
        return hoaDonBan;
    }

    public void setHoaDonBan(HoaDonBan hoaDonBan) {
        this.hoaDonBan = hoaDonBan;
    }

    public MonAn getMonAn() {
        return monAn;
    }

    public void setMonAn(MonAn monAn) {
        this.monAn = monAn;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }


}
