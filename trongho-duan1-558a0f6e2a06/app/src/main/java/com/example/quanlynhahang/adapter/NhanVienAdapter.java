package com.example.quanlynhahang.adapter;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quanlynhahang.QuanLyNhanVienActivity;
import com.example.quanlynhahang.R;
import com.example.quanlynhahang.dao.NhanVienDAO;
import com.example.quanlynhahang.model.NhanVien;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;

public class NhanVienAdapter extends RecyclerView.Adapter<NhanVienAdapter.ViewHolder> {
    Context context;
    List<NhanVien> list;
    NhanVien nhanVien;

    public NhanVienAdapter(Context context,List<NhanVien> list) {
        this.context=context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvTenNV,tvEmail;
        public ImageView ivAnhDaiDien,ivDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivAnhDaiDien=itemView.findViewById(R.id.imgAnhDaiDien);
            tvTenNV=itemView.findViewById(R.id.tvTenNV);
            tvEmail=itemView.findViewById(R.id.tvEmail);
            ivDelete=itemView.findViewById(R.id.ivDelete);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    nhanVien=list.get(getAdapterPosition());
                    ((QuanLyNhanVienActivity)context).suaNhanVien(nhanVien);
                }
            });

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    nhanVien=list.get(getAdapterPosition());
                    ((QuanLyNhanVienActivity)context).xoaNhanVien(nhanVien);
                }
            });

        }

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Tao view va gan layout vao view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.one_cell_nhan_vien, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        nhanVien=list.get(i);

        viewHolder.tvTenNV.setText(nhanVien.getTenNV());
        viewHolder.tvEmail.setText(nhanVien.getEmail());
        viewHolder.ivAnhDaiDien.setImageResource(R.drawable.avatar1);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
