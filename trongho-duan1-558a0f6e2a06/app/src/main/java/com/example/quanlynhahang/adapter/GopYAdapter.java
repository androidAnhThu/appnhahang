package com.example.quanlynhahang.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quanlynhahang.QuanLyGopYActivity;
import com.example.quanlynhahang.R;
import com.example.quanlynhahang.model.GopY;

import java.util.List;

public class GopYAdapter extends RecyclerView.Adapter<GopYAdapter.ViewHolder> {
    Context context;
    List<GopY> list;
    GopY gopY;

    public GopYAdapter(Context context,List<GopY> list) {
        this.context=context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvThoiGian,tvEmail,tvNoiDung;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvThoiGian=itemView.findViewById(R.id.tvThoiGian);
            tvEmail=itemView.findViewById(R.id.tvEmail);
            tvNoiDung=itemView.findViewById(R.id.tvNoiDung);
        }

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Tao view va gan layout vao view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.one_cell_gop_y, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        gopY=list.get(i);

        viewHolder.tvThoiGian.setText(gopY.getThoiGian());
        viewHolder.tvEmail.setText(gopY.getEmail());
        viewHolder.tvNoiDung.setText(gopY.getNoiDung());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

