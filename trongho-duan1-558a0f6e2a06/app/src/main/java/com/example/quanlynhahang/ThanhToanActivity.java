package com.example.quanlynhahang;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.quanlynhahang.adapter.HoaDonBanAdapter;
import com.example.quanlynhahang.adapter.HoaDonBanChiTietAdapter;
import com.example.quanlynhahang.dao.HoaDonBanChiTietDAO;
import com.example.quanlynhahang.dao.HoaDonBanDAO;
import com.example.quanlynhahang.model.HoaDonBan;
import com.example.quanlynhahang.model.HoaDonBanChiTiet;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ThanhToanActivity extends AppCompatActivity {

    RecyclerView rv_dshdb,rv_dshdbct;
    Context c;

    HoaDonBan hoaDonBan;
    List<HoaDonBan> list;
    HoaDonBanDAO hoaDonBanDAO;
    HoaDonBanAdapter hoaDonBanAdapter;

    HoaDonBanChiTiet hoaDonBanChiTiet;
    List<HoaDonBanChiTiet> list2;
    HoaDonBanChiTietDAO hoaDonBanChiTietDAO;
    HoaDonBanChiTietAdapter hoaDonBanChiTietAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thanh_toan);

        rv_dshdb=findViewById(R.id.rv_dshdb);
        rv_dshdb.setHasFixedSize(true);
        rv_dshdb.setLayoutManager(new LinearLayoutManager(c));

        rv_dshdbct=findViewById(R.id.rv_dshdbct);
        rv_dshdbct.setHasFixedSize(true);
        rv_dshdbct.setLayoutManager(new LinearLayoutManager(c));

        showHDB();
        showHDBCT();
    }

    public void showHDB(){
        Intent intent=getIntent();
        final String maBan=intent.getStringExtra("ma_ban");

        final DatabaseReference mDatabase= FirebaseDatabase.getInstance().getReference("HoaDonBan");
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if(data.child("maBan").getValue(String.class).equals(maBan)) {
                        HoaDonBan hoaDonBan = data.getValue(HoaDonBan.class);
                        list=new ArrayList<>();
                        list.add(hoaDonBan);
                        hoaDonBanAdapter=new HoaDonBanAdapter(ThanhToanActivity.this,list);
                        rv_dshdb.setAdapter(hoaDonBanAdapter);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);


    }

    public void showHDBCT(){
        list2=new ArrayList<>();
        hoaDonBanChiTietDAO=new HoaDonBanChiTietDAO(ThanhToanActivity.this);
        list2=hoaDonBanChiTietDAO.getAll();
        hoaDonBanChiTietAdapter=new HoaDonBanChiTietAdapter(ThanhToanActivity.this,list2);
        rv_dshdbct.setAdapter(hoaDonBanChiTietAdapter);
    }
}
