package com.example.quanlynhahang.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quanlynhahang.ChonMonAnActivity;
import com.example.quanlynhahang.MonDaChonFragment;
import com.example.quanlynhahang.R;
import com.example.quanlynhahang.dao.ChiTietBanDAO;
import com.example.quanlynhahang.model.ChiTietBan;
import com.example.quanlynhahang.model.MonAn;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MonDaChonAdapter extends RecyclerView.Adapter<MonDaChonAdapter.ViewHolder> {
    Context context;
    List<ChiTietBan> list;
    ChiTietBan chiTietBan;
    MonDaChonFragment monDaChonFragment;

    public MonDaChonAdapter(Context context,List<ChiTietBan> list) {
        this.context=context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvTenMonAn,tvGiaTien;
        public EditText edtSoLuong;
        public ImageView ivAnhMonAn,ivDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivAnhMonAn=itemView.findViewById(R.id.imgAnhMonAn);
            tvTenMonAn=itemView.findViewById(R.id.tvTenMonAn);
            tvGiaTien=itemView.findViewById(R.id.tvGiaTien);
            edtSoLuong=itemView.findViewById(R.id.edtSoLuong);
            ivDelete=itemView.findViewById(R.id.ivDelete);

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chiTietBan=list.get(getAdapterPosition());
                    ChiTietBanDAO chiTietBanDAO=new ChiTietBanDAO(context);
                    chiTietBanDAO.delete(chiTietBan);
                    MonDaChonAdapter.this.notifyDataSetChanged();
                }
            });

        }

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Tao view va gan layout vao view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.one_cell_mon_da_chon, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        chiTietBan=list.get(i);

        viewHolder.tvTenMonAn.setText(chiTietBan.getMonAn().getTenMonAn());
        viewHolder.tvGiaTien.setText(chiTietBan.getMonAn().getGiaTien()+"");
        viewHolder.edtSoLuong.setText(chiTietBan.getSoLuong()+"");
        Picasso.with(context).load(chiTietBan.getMonAn().getImageURL()).into(viewHolder.ivAnhMonAn);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

