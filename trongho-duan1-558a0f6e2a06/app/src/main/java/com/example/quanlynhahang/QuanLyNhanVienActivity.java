package com.example.quanlynhahang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.quanlynhahang.adapter.NhanVienAdapter;
import com.example.quanlynhahang.dao.NhanVienDAO;
import com.example.quanlynhahang.model.NhanVien;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class QuanLyNhanVienActivity extends AppCompatActivity {
    RecyclerView rv_dsnv;
    Context c;
    FloatingActionButton fl_them;

    List<NhanVien> list;
    NhanVienDAO nhanVienDAO;
    NhanVienAdapter nhanVienAdapter;

    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mReferce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quan_ly_nhan_vien);

        rv_dsnv=findViewById(R.id.rv_dsnv);
        rv_dsnv.setHasFixedSize(true);
        rv_dsnv.setLayoutManager(new LinearLayoutManager(c));
        showDSNV();

        fl_them=findViewById(R.id.fl_them);
        themNhanVien();

        mDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mReferce = mDatabase.getReference();
    }

    public void showDSNV(){
        nhanVienDAO=new NhanVienDAO(QuanLyNhanVienActivity.this);
        list=new ArrayList<NhanVien>();
        list=nhanVienDAO.getAll();
        nhanVienAdapter=new NhanVienAdapter(QuanLyNhanVienActivity.this,list);
        rv_dsnv.setAdapter(nhanVienAdapter);
    }

    public void xoaNhanVien(NhanVien nhanVien){
        nhanVienDAO.delete(nhanVien);
        capNhatLV();
    }

    public void suaNhanVien(final NhanVien nhanVien){
        final Dialog dialog = new Dialog(QuanLyNhanVienActivity.this);
        dialog.setContentView(R.layout.sua_nhanvien_layout);

        final EditText edtTenNhanVien=dialog.findViewById(R.id.edtTenNhanVien);
        final EditText edtEmail=dialog.findViewById(R.id.edtEmail);
        final EditText edtPassword=dialog.findViewById(R.id.edtPassword);
        final EditText edtRePassword=dialog.findViewById(R.id.edtRePassword);
        final Button btnSua=dialog.findViewById(R.id.btnSua);

        edtTenNhanVien.setText(nhanVien.getTenNV());
        edtEmail.setText(nhanVien.getEmail());
        edtPassword.setText(nhanVien.getPassword());
        edtRePassword.setText(nhanVien.getPassword());

        btnSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String tenNV=edtTenNhanVien.getText().toString();
                final String email=edtEmail.getText().toString();
                final String pw=edtPassword.getText().toString();
                final String rpw=edtRePassword.getText().toString();
                if (TextUtils.isEmpty(tenNV)) {
                    Toast.makeText(QuanLyNhanVienActivity.this, "mời nhập tên nhân viên...", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(QuanLyNhanVienActivity.this, "mời nhập email...", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(pw)) {
                    Toast.makeText(QuanLyNhanVienActivity.this, "mời nhập mật khẩu...", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(rpw)) {
                    Toast.makeText(QuanLyNhanVienActivity.this, "mời nhập lại mật khẩu...", Toast.LENGTH_LONG).show();
                    return;
                }
                if (!rpw.equals(pw)) {
                    Toast.makeText(QuanLyNhanVienActivity.this, "nhập lại mật khẩu  không trùng...", Toast.LENGTH_LONG).show();
                    return;
                }

                NhanVien nhanVien=new NhanVien();
                nhanVienDAO=new NhanVienDAO(QuanLyNhanVienActivity.this);
                nhanVien.setTenNV(edtTenNhanVien.getText().toString());
                nhanVien.setEmail(edtEmail.getText().toString());
                nhanVien.setPassword(edtPassword.getText().toString());
                nhanVienDAO.update(nhanVien);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void capNhatLV(){
        nhanVienAdapter.notifyItemInserted(list.size());
        nhanVienAdapter.notifyDataSetChanged();
    }

    public void themNhanVien(){
        fl_them.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(QuanLyNhanVienActivity.this);
                dialog.setContentView(R.layout.them_nhan_vien_layout);

                final EditText edtTenNhanVien=dialog.findViewById(R.id.edtTenNhanVien);
                final EditText edtEmail=dialog.findViewById(R.id.edtEmail);
                final EditText edtPassword=dialog.findViewById(R.id.edtPassword);
                final EditText edtRePassword=dialog.findViewById(R.id.edtRePassword);
                final Button btnThem=dialog.findViewById(R.id.btnThem);

                btnThem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String tenNV=edtTenNhanVien.getText().toString();
                        final String email=edtEmail.getText().toString();
                        final String pw=edtPassword.getText().toString();
                        final String rpw=edtRePassword.getText().toString();
                        if (TextUtils.isEmpty(tenNV)) {
                            Toast.makeText(getApplicationContext(), "mời nhập tên nhân viên...", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (TextUtils.isEmpty(email)) {
                            Toast.makeText(getApplicationContext(), "mời nhập email...", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (TextUtils.isEmpty(pw)) {
                            Toast.makeText(getApplicationContext(), "mời nhập mật khẩu...", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (TextUtils.isEmpty(rpw)) {
                            Toast.makeText(getApplicationContext(), "mời nhập lại mật khẩu...", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (!rpw.equals(pw)) {
                            Toast.makeText(getApplicationContext(), "nhập lại mật khẩu  không trùng...", Toast.LENGTH_LONG).show();
                            return;
                        }

                        mAuth.createUserWithEmailAndPassword(email, pw)
                                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            final String userUID = mAuth.getCurrentUser().getUid();
                                            final DatabaseReference mUser = mDatabase.getReference().child("NhanVien");

                                            //lưu thông tin nhân viên vào database
                                            mUser.child(userUID).child("tenNV").setValue(tenNV);
                                            mUser.child(userUID).child("email").setValue(email);
                                            mUser.child(userUID).child("password").setValue(pw);

                                            Toast.makeText(getApplicationContext(), "Thêm nhân viên thành công", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                        }
                                        else {
                                            Toast.makeText(getApplicationContext(), "Thêm thất bại, vui lòng thử lại", Toast.LENGTH_SHORT).show();

                                        }
                                    }
                                });
                    }
                });
                dialog.show();

            }
        });
    }
}
