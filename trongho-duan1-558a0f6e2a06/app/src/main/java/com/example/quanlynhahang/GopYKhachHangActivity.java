package com.example.quanlynhahang;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.quanlynhahang.dao.GopYDao;
import com.example.quanlynhahang.model.GopY;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class GopYKhachHangActivity extends AppCompatActivity {
    EditText edtNoiDung;
    Button btnGui;
    GopY gopY;
    GopYDao gopYDao;
    static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gop_ykhach_hang);

        edtNoiDung=findViewById(R.id.edtNoiDung);
        btnGui=findViewById(R.id.btnGui);

        btnGui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gopY=new GopY();
                gopYDao=new GopYDao(GopYKhachHangActivity.this);

                Random r=new Random();
                gopY.setMaGopY(String.valueOf(r.nextInt(1000000)));
                gopY.setNoiDung(edtNoiDung.getText().toString());

                Calendar c = Calendar.getInstance();
                String strDate = sdf.format(c.getTime());
                gopY.setThoiGian(strDate);

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                String email=user.getEmail();
                gopY.setEmail(email);

                gopYDao.insert(gopY);
                finish();
            }
        });
    }
}
