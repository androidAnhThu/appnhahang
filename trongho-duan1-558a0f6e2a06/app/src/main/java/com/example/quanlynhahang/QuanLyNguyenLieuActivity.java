package com.example.quanlynhahang;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.quanlynhahang.adapter.NguyenLieuAdapter;
import com.example.quanlynhahang.dao.NguyenLieuDao;
import com.example.quanlynhahang.model.NguyenLieu;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class QuanLyNguyenLieuActivity extends AppCompatActivity {
    RecyclerView rv_dsnl;
    Context c;
    FloatingActionButton fl_them;

    NguyenLieu nguyenLieu;
    List<NguyenLieu> list;
    NguyenLieuDao nguyenLieuDao;
    NguyenLieuAdapter nguyenLieuAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quan_ly_nguyen_lieu);

        rv_dsnl=findViewById(R.id.rv_dsnl);
        rv_dsnl.setHasFixedSize(true);
        rv_dsnl.setLayoutManager(new LinearLayoutManager(c));

        fl_them=findViewById(R.id.fl_them);
        themNguyenLieu();

        showDSGY();
    }



    public void showDSGY(){
        nguyenLieuDao=new NguyenLieuDao(QuanLyNguyenLieuActivity.this);
        list=new ArrayList<NguyenLieu>();
        list=nguyenLieuDao.getAll();
        nguyenLieuAdapter=new NguyenLieuAdapter(QuanLyNguyenLieuActivity.this,list);
        rv_dsnl.setAdapter(nguyenLieuAdapter);
    }

    public void capNhatLV(){
        nguyenLieuAdapter.notifyItemInserted(list.size());
        nguyenLieuAdapter.notifyDataSetChanged();
    }

    public void themNguyenLieu(){
        fl_them.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(QuanLyNguyenLieuActivity.this);
                dialog.setContentView(R.layout.them_nguyen_lieu_layout);

                final EditText edtMaNguyenLieu=dialog.findViewById(R.id.edtMaNguyenLieu);
                final EditText edtTenNguyenLieu=dialog.findViewById(R.id.edtTenNguyenLieu);
                final Button btnThem=dialog.findViewById(R.id.btnThem);

                btnThem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String maNL=edtMaNguyenLieu.getText().toString();
                        final String tenNL=edtTenNguyenLieu.getText().toString();

                        if (TextUtils.isEmpty(maNL)) {
                            Toast.makeText(getApplicationContext(), "mời nhập mã nguyên liệu...", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (TextUtils.isEmpty(tenNL)) {
                            Toast.makeText(getApplicationContext(), "mời nhập tên nguyên liêu...", Toast.LENGTH_LONG).show();
                            return;
                        }

                        nguyenLieu=new NguyenLieu();
                        nguyenLieuDao=new NguyenLieuDao(QuanLyNguyenLieuActivity.this);
                        nguyenLieu.setMaNguyenLieu(edtMaNguyenLieu.getText().toString());
                        nguyenLieu.setTenNguyenLieu(edtTenNguyenLieu.getText().toString());
                        nguyenLieuDao.insert(nguyenLieu);
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }
}
