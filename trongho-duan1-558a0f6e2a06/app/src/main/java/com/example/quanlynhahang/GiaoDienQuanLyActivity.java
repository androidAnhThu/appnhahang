package com.example.quanlynhahang;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GiaoDienQuanLyActivity extends AppCompatActivity {
    Button btn_qlkh,btn_qlnv,btn_qlgy,btn_qlnl,btn_qlma,btn_qlb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_giao_dien_quan_ly);

        btn_qlkh=findViewById(R.id.btn_qlkh);
        btn_qlnv=findViewById(R.id.btn_qlnv);
        btn_qlgy=findViewById(R.id.btn_qlgy);
        btn_qlnl=findViewById(R.id.btn_qlnl);
        btn_qlma=findViewById(R.id.btn_qlma);
        btn_qlb=findViewById(R.id.btn_qlb);

        quanLyKhachHang();
        quanLyNhanVien();
        quanLyGopY();
        quanLyNguyenLieu();
        quanLyMonAn();
        quanLyBan();
    }

    public void quanLyKhachHang(){
        btn_qlkh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GiaoDienQuanLyActivity.this,QuanLyKhachHangActivity.class);
                startActivity(intent);
            }
        });
    }

    public void quanLyNhanVien(){
        btn_qlnv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GiaoDienQuanLyActivity.this,QuanLyNhanVienActivity.class);
                startActivity(intent);
            }
        });
    }

    public void quanLyGopY(){
        btn_qlgy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GiaoDienQuanLyActivity.this,QuanLyGopYActivity.class);
                startActivity(intent);
            }
        });
    }

    public void quanLyNguyenLieu(){
        btn_qlnl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GiaoDienQuanLyActivity.this,QuanLyNguyenLieuActivity.class);
                startActivity(intent);
            }
        });
    }

    public void quanLyMonAn(){
        btn_qlma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GiaoDienQuanLyActivity.this,QuanLyMonAnActivity.class);
                startActivity(intent);
            }
        });
    }

    public void quanLyBan(){
        btn_qlb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GiaoDienQuanLyActivity.this,QuanLyBanActivity.class);
                startActivity(intent);
            }
        });
    }
}
