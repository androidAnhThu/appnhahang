package com.example.quanlynhahang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.quanlynhahang.model.KhachHang;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class ThongTinKhachHangActivity extends AppCompatActivity {
    private static final String TAG = "";
    ImageView imgAnhDaiDien;
    TextView tvTenKH,tvEmail,tvPassword,tvSDT,tvDiaChi;
    Button btnDoiMatKhau;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thong_tin_khach_hang);

        imgAnhDaiDien=findViewById(R.id.imgAnhDaiDien);
        tvTenKH=findViewById(R.id.tvTenKH);
        tvEmail=findViewById(R.id.tvEmail);
        tvSDT=findViewById(R.id.tvSDT);
        tvDiaChi=findViewById(R.id.tvDiaChi);
        tvPassword=findViewById(R.id.tvPassword);
        btnDoiMatKhau=findViewById(R.id.btnDoiMatKhau);

        thongTinKhachHang();
        doiMatKhau();
    }

    public void thongTinKhachHang(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {

            String email = user.getEmail();
            final String uid = user.getUid();

            final DatabaseReference mUser=FirebaseDatabase.getInstance().getReference("KhachHang");
            ValueEventListener postListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // Get Post object and use the values to update the UI
                    KhachHang khachHang = dataSnapshot.getValue(KhachHang.class);
                    String ten=khachHang.getTenKH();
                    String pw=khachHang.getPassword();
                    String dc=khachHang.getDiaChi();
                    String sdt=khachHang.getSdt();
                    String imgURL=khachHang.getImageURL();
                    tvPassword.setText(pw);
                    tvTenKH.setText(ten);
                    tvSDT.setText(sdt);
                    tvDiaChi.setText(dc);

                    //Load ảnh đại diện
                    Picasso.with(ThongTinKhachHangActivity.this)
                            .load(imgURL)
                            .placeholder(R.drawable.ic_launcher_background) // optional
                            .error(R.drawable.ic_launcher_foreground)         // optional
                            .into(imgAnhDaiDien);
                    // ...
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Getting Post failed, log a message
                    Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                    // ...
                }
            };
            mUser.child(uid).addValueEventListener(postListener);
            tvEmail.setText(email);
        }
    }

    public void doiMatKhau(){
        btnDoiMatKhau.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(ThongTinKhachHangActivity.this);
                dialog.setContentView(R.layout.doi_mat_khau_layout);

                final EditText edtPassword = dialog.findViewById(R.id.edtPassword);
                final EditText edtNewPassword = dialog.findViewById(R.id.edtNewPassword);
                final EditText edtReNewPassword = dialog.findViewById(R.id.edtReNewPassword);
                Button btnDoiMatKhau = dialog.findViewById(R.id.btnDoiMatKhau);

                btnDoiMatKhau.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String pw=edtPassword.getText().toString();
                        final String npw=edtNewPassword.getText().toString();
                        String rnpw=edtReNewPassword.getText().toString();
                        if (TextUtils.isEmpty(pw)) {
                            Toast.makeText(getApplicationContext(), "mời nhập mật khẩu...", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (TextUtils.isEmpty(npw)) {
                            Toast.makeText(getApplicationContext(), "mời nhập mật khẩu mới...", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (TextUtils.isEmpty(rnpw)) {
                            Toast.makeText(getApplicationContext(), "mời nhập lại mật khẩu mới...", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (!rnpw.equals(npw)) {
                            Toast.makeText(getApplicationContext(), "nhập lại mật khẩu mới không trùng...", Toast.LENGTH_LONG).show();
                            return;
                        }

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        final String userUID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        final DatabaseReference mUser = FirebaseDatabase.getInstance().getReference().child("KhachHang");

                        user.updatePassword(npw)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            mUser.child(userUID).child("password").setValue(npw);
                                            Toast.makeText(ThongTinKhachHangActivity.this,"Đổi mật khẩu thành công",Toast.LENGTH_SHORT).show();
                                            Log.d(TAG, "User password updated.");
                                        }
                                    }
                                });
                    }
                });
                dialog.show();
            }
        });
    }
}
