package com.example.quanlynhahang;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.quanlynhahang.adapter.MonAnAdapter;
import com.example.quanlynhahang.dao.MonAnDao;
import com.example.quanlynhahang.model.MonAn;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class QuanLyMonAnActivity extends AppCompatActivity {
    RecyclerView rv_dsma;
    Context c;
    FloatingActionButton fl_them;

    List<MonAn> list;
    MonAnDao monAnDao;
    MonAnAdapter monAnAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quan_ly_mon_an);

        rv_dsma=findViewById(R.id.rv_dsma);
        rv_dsma.setHasFixedSize(true);
        rv_dsma.setLayoutManager(new LinearLayoutManager(c));
        showDSMA();

        fl_them=findViewById(R.id.fl_them);
        themMonAn();
    }

    public void showDSMA(){
        monAnDao=new MonAnDao(QuanLyMonAnActivity.this);
        list=new ArrayList<MonAn>();
        list=monAnDao.getAll();
        monAnAdapter=new MonAnAdapter(QuanLyMonAnActivity.this,list);
        rv_dsma.setAdapter(monAnAdapter);

    }

    public void capNhatLV(){
        monAnAdapter.notifyItemInserted(list.size());
        monAnAdapter.notifyDataSetChanged();
    }

    public void themMonAn(){
        fl_them.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(QuanLyMonAnActivity.this,ThemMonAnActivity.class);
                startActivity(intent);

            }
        });
    }

}
