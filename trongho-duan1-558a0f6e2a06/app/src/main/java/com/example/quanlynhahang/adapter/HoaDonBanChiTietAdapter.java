package com.example.quanlynhahang.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quanlynhahang.R;
import com.example.quanlynhahang.model.HoaDonBan;
import com.example.quanlynhahang.model.HoaDonBanChiTiet;

import java.util.List;

public class HoaDonBanChiTietAdapter extends RecyclerView.Adapter<HoaDonBanChiTietAdapter.ViewHolder> {
    Context context;
    List<HoaDonBanChiTiet> list;
    HoaDonBanChiTiet hoaDonBanChiTiet;

    public HoaDonBanChiTietAdapter(Context context,List<HoaDonBanChiTiet> list) {
        this.context=context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvTenMonAn,tvSoLuong,tvGiaTien,tvThanhTien;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTenMonAn=itemView.findViewById(R.id.tvTenMonAn);
            tvSoLuong=itemView.findViewById(R.id.tvSoLuong);
            tvGiaTien=itemView.findViewById(R.id.tvGiaTien);
            tvThanhTien=itemView.findViewById(R.id.tvThanhTien);
        }

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Tao view va gan layout vao view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.one_cell_hoa_don_ban_chi_tiet, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        hoaDonBanChiTiet=list.get(i);

        viewHolder.tvTenMonAn.setText(hoaDonBanChiTiet.getMonAn().getTenMonAn());
        viewHolder.tvSoLuong.setText(hoaDonBanChiTiet.getSoLuong());
        viewHolder.tvGiaTien.setText(hoaDonBanChiTiet.getMonAn().getGiaTien());
        viewHolder.tvThanhTien.setText(hoaDonBanChiTiet.getMonAn().getGiaTien()*hoaDonBanChiTiet.getSoLuong());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

