package com.example.quanlynhahang;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.quanlynhahang.model.HoaDonNhapChiTiet;
import com.example.quanlynhahang.model.NguyenLieu;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class NhapNguyenLieuActivity extends AppCompatActivity {

    EditText edtMaNV, edtMaNguyenLieu, edtMaHD, edtSoLuong, edtGiaTien;
    Button btnNhap, btnXoa;
    HoaDonNhapChiTiet hoaDonNhapChiTiet;
    NguyenLieu nguyenLieu;
    DatabaseReference mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nhap_nguyen_lieu);

        //ánh xạ
        edtMaNV = findViewById(R.id.edtMaNV);
        edtMaNguyenLieu = findViewById(R.id.edtMaNguyenLieu);
        edtMaHD = findViewById(R.id.edtMaHD);
        edtSoLuong = findViewById(R.id.edtSoLuong);
        edtGiaTien = findViewById(R.id.edtGiaTien);
        btnNhap = findViewById(R.id.btnNhapNguyenLieu);
        btnXoa = findViewById(R.id.btnXoaNguyenLieu);
        mData = FirebaseDatabase.getInstance().getReference("HoaDonNhapChiTiet");

        mData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        btnNhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nhapArrayList();
                Cleartxt();
            }
        });

        btnXoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cleartxt();
            }
        });

    }

    private void Cleartxt(){
        edtMaNV.setText("");
        edtMaHD.setText("");
        edtMaNguyenLieu.setText("");
        edtSoLuong.setText("");
        edtGiaTien.setText("");
        edtMaNV.requestFocus();
    }

    private void  nhapArrayList() {
        final String maNV = edtMaNV.getText().toString().trim();
        final String maNguyenLieu = edtMaNguyenLieu.getText().toString().trim();
        final String maHD = edtMaHD.getText().toString().trim();
        final int soLuong = Integer.parseInt(edtSoLuong.getText().toString().trim());
        final int gia = Integer.parseInt(edtGiaTien.getText().toString().trim());

        if (TextUtils.isEmpty(maNV)) {
            edtMaNV.setError("Bạn chưa nhập Mã Nhân Viên!");
        } else if (TextUtils.isEmpty(maNguyenLieu)) {
            edtMaNguyenLieu.setError("Bạn chưa nhập Mã Nguyên Liệu!");
        } else if (TextUtils.isEmpty(maNguyenLieu)) {
            edtMaNguyenLieu.setError("Bạn chưa nhập Mã Hóa Đơn!");
        } else if (TextUtils.isEmpty(edtSoLuong.toString())) {
            edtSoLuong.setError("Bạn chưa nhập Số Lượng!");
        } else if (TextUtils.isEmpty(edtGiaTien.toString())) {
            edtGiaTien.setError("Bạn chưa nhập Giá Tiền!");
        } else {

            HoaDonNhapChiTiet hoaDonNhapChiTiet = new HoaDonNhapChiTiet(maNV, maNguyenLieu, maHD, soLuong, gia);
            mData.child("HoaDonChiTiet").child(maNguyenLieu).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    mData = FirebaseDatabase.getInstance().getReference();
                    mData.child("HoaDonNhapChiTiet").child(maNguyenLieu).child("maNV").setValue(maNV);
                    mData.child("HoaDonNhapChiTiet").child(maNguyenLieu).child("maHoaDon").setValue(maHD);
                    mData.child("HoaDonNhapChiTiet").child(maNguyenLieu).child("soLuong").setValue(soLuong);
                    mData.child("HoaDonNhapChiTiet").child(maNguyenLieu).child("gia").setValue(gia);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            Toast.makeText(this, "Nhập nguyên liệu thành công.", Toast.LENGTH_LONG).show();
            Cleartxt();

        }
    }
}
