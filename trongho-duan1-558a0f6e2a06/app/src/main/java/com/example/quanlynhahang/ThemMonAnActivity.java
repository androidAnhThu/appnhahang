package com.example.quanlynhahang;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.quanlynhahang.dao.MonAnDao;
import com.example.quanlynhahang.model.LoaiMonAn;
import com.example.quanlynhahang.model.MonAn;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ThemMonAnActivity extends AppCompatActivity {
    Button btnChonHinh,btnThem;
    ImageView imgAnhMonAn;
    Spinner spMaLoai;
    EditText edtMaLoai,edtMaMonAn,edtTenMonAn,edtMoTa,edtGiaTien;

    private static final int PICK_IMAGE_REQUEST = 234;
    Uri imageUri;

    private DatabaseReference mDatabase;
    StorageReference storageReference;

    public String image_url;
    String id= "";

    MonAn monAn;
    MonAnDao monAnDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_them_mon_an);

        btnChonHinh=findViewById(R.id.btnChonHinh);
        imgAnhMonAn=findViewById(R.id.imgAnhMonAn);
        spMaLoai=findViewById(R.id.spMaLoai);
        edtMaLoai=findViewById(R.id.edtMaLoai);
        edtMaMonAn=findViewById(R.id.edtMaMonAn);
        edtTenMonAn=findViewById(R.id.edtTenMonAn);
        edtMoTa=findViewById(R.id.edtMoTa);
        edtGiaTien=findViewById(R.id.edtGiaTien);
        btnThem=findViewById(R.id.btnThem);

        chonHinhMonAn();
        spinnerLoaiMonAn();
        themMonAn();
    }

    public void chonHinhMonAn(){
        btnChonHinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select image"), PICK_IMAGE_REQUEST);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){
            imageUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(ThemMonAnActivity.this.getContentResolver(), imageUri);
                imgAnhMonAn.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void spinnerLoaiMonAn(){
        //spinner chọn loại món ăn
        mDatabase = FirebaseDatabase.getInstance().getReference("LoaiMonAn");
        final List<String> list=new ArrayList<>();
        ValueEventListener postListener=new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot data:dataSnapshot.getChildren()){
                    String ma=data.child("maLoai").getValue(String.class);
                    list.add(ma);

                    ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(
                            ThemMonAnActivity.this,android.R.layout.simple_spinner_item,list);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spMaLoai.setAdapter(arrayAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        mDatabase.addValueEventListener(postListener);
    }

    public void themMonAn(){
        btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edtMaMonAn.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Vui long nhap mã món ăn", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(edtTenMonAn.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Vui long nhap tên món ăn", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(edtMoTa.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Vui long nhap mô tả", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(edtGiaTien.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Vui long nhap giá tiền", Toast.LENGTH_SHORT).show();
                    return;
                }

                monAn=new MonAn();
                monAnDao=new MonAnDao(ThemMonAnActivity.this);


                //lưu ảnh món ăn vào storage
                storageReference = FirebaseStorage.getInstance().getReference("AnhMonAn");
                if (imageUri != null){
                    StorageReference picRef = storageReference.child(imageUri.getLastPathSegment());
                    picRef.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            Task<Uri> urlTak = taskSnapshot.getStorage().getDownloadUrl();
                            while (!urlTak.isSuccessful());
                            Uri downloadUrl = urlTak.getResult();
                            image_url = downloadUrl.toString();


                            monAn.setMaLoai(spMaLoai.getSelectedItem().toString());
                            monAn.setMaMonAn(edtMaMonAn.getText().toString());
                            monAn.setTenMonAn(edtTenMonAn.getText().toString());
                            monAn.setMoTa(edtMoTa.getText().toString());
                            monAn.setGiaTien(Integer.parseInt(edtGiaTien.getText().toString()));
                            monAn.setImageURL(image_url);

                            monAnDao.insert(monAn);
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                        }
                    });
                }

            }
        });
    }
}
