package com.example.quanlynhahang;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;

import com.example.quanlynhahang.adapter.GopYAdapter;
import com.example.quanlynhahang.dao.GopYDao;
import com.example.quanlynhahang.model.GopY;

import java.util.ArrayList;
import java.util.List;

public class QuanLyGopYActivity extends AppCompatActivity {
    RecyclerView rv_dsgy;
    Context c;

    List<GopY> list;
    GopYDao gopYDao;
    GopYAdapter gopYAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quan_ly_gop_y);

        rv_dsgy=findViewById(R.id.rv_dsgy);
        rv_dsgy.setHasFixedSize(true);
        rv_dsgy.setLayoutManager(new LinearLayoutManager(c));

        showDSGY();
    }

    public void showDSGY(){
        gopYDao=new GopYDao(QuanLyGopYActivity.this);
        list=new ArrayList<GopY>();
        list=gopYDao.getAll();
        gopYAdapter=new GopYAdapter(QuanLyGopYActivity.this,list);
        rv_dsgy.setAdapter(gopYAdapter);
    }

    public void capNhatLV(){
        gopYAdapter.notifyItemInserted(list.size());
        gopYAdapter.notifyDataSetChanged();
    }
}
