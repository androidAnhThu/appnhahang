package com.example.quanlynhahang.model;

public class MonAn {
    String maMonAn,tenMonAn,moTa,maLoai,imageURL;
    int giaTien;

    public MonAn(String maMonAn, String tenMonAn, String moTa, String maLoai, String imageURL, int giaTien) {
        this.maMonAn = maMonAn;
        this.tenMonAn = tenMonAn;
        this.moTa = moTa;
        this.maLoai = maLoai;
        this.imageURL = imageURL;
        this.giaTien = giaTien;
    }

    public MonAn() {
    }

    public String getMaMonAn() {
        return maMonAn;
    }

    public void setMaMonAn(String maMonAn) {
        this.maMonAn = maMonAn;
    }

    public String getTenMonAn() {
        return tenMonAn;
    }

    public void setTenMonAn(String tenMonAn) {
        this.tenMonAn = tenMonAn;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getMaLoai() {
        return maLoai;
    }

    public void setMaLoai(String maLoai) {
        this.maLoai = maLoai;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public int getGiaTien() {
        return giaTien;
    }

    public void setGiaTien(int giaTien) {
        this.giaTien = giaTien;
    }
}
