package com.example.quanlynhahang.model;

public class HoaDonBan {
    String maHD;
    NhanVien nhanVien;
    String maBan;
    String thoiGian;

    public HoaDonBan(String maHD, NhanVien nhanVien,String maBan,String thoiGian) {
        this.maHD = maHD;
        this.nhanVien=nhanVien;
        this.maBan=maBan;
        this.thoiGian = thoiGian;
    }

    public HoaDonBan() {
    }

    public String getMaHD() {
        return maHD;
    }

    public void setMaHD(String maHD) {
        this.maHD = maHD;
    }

    public NhanVien getNhanVien() {
        return nhanVien;
    }

    public void setNhanVien(NhanVien nhanVien) {
        this.nhanVien = nhanVien;
    }

    public String getMaBan() {
        return maBan;
    }

    public void setMaBan(String maBan) {
        this.maBan = maBan;
    }

    public String getThoiGian() {
        return thoiGian;
    }

    public void setThoiGian(String thoiGian) {
        this.thoiGian = thoiGian;
    }
}
