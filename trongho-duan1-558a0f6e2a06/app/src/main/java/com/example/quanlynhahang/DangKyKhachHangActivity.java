package com.example.quanlynhahang;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.quanlynhahang.model.KhachHang;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.IOException;

public class DangKyKhachHangActivity extends AppCompatActivity {
    EditText edtEmail,edtPassword,edtRePassword,edtTen,edtSDT,edtDiaChi;
    ImageView imgHinhDaiDien;
    Button btnChonHinh,btnDangKy,btnNhapLai;

    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mReferce;
    StorageReference storageReference;

    public String image_url;
    String id= "";

    private static final int PICK_IMAGE_REQUEST = 234;
    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dang_ky_khach_hang);

        edtEmail=findViewById(R.id.edtEmail);
        edtPassword=findViewById(R.id.edtPassword);
        edtRePassword=findViewById(R.id.edtRePassword);
        edtTen=findViewById(R.id.edtTen);
        edtSDT=findViewById(R.id.edtSDT);
        edtDiaChi=findViewById(R.id.edtDiaChi);
        imgHinhDaiDien=findViewById(R.id.imgAvatar);
        btnChonHinh=findViewById(R.id.btnChonHinh);
        btnDangKy=findViewById(R.id.btnDangKy);
        btnNhapLai=findViewById(R.id.btnNhapLai);

        mDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mReferce = mDatabase.getReference();

        chonHinhDaiDien();
        dangKy();
        nhapLai();

    }

    public void chonHinhDaiDien(){
        btnChonHinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select image"), PICK_IMAGE_REQUEST);
            }
        });
    }

    public void luuHinhDaiDien(){

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){
            imageUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(DangKyKhachHangActivity.this.getContentResolver(), imageUri);
                imgHinhDaiDien.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void dangKy(){
        btnDangKy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = edtEmail.getText().toString();
                final String password = edtPassword.getText().toString();
                final String rePassword = edtRePassword.getText().toString();
                final String ten=edtTen.getText().toString();
                final String sdt=edtSDT.getText().toString();
                final String diaChi=edtDiaChi.getText().toString();
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Vui long nhap Email", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Vui long nhap password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(rePassword)) {
                    Toast.makeText(getApplicationContext(), "Vui long nhap lại password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!password.equals(rePassword)) {
                    Toast.makeText(getApplicationContext(), "Vui lòng nhập lại đúng password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(ten)) {
                    Toast.makeText(getApplicationContext(), "Vui long nhap họ tên", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(sdt)) {
                    Toast.makeText(getApplicationContext(), "Vui long nhap số điện thoại", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(diaChi)) {
                    Toast.makeText(getApplicationContext(), "Vui long nhap địa chỉ", Toast.LENGTH_SHORT).show();
                    return;
                }

                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    final String userUID = mAuth.getCurrentUser().getUid();
                                    final DatabaseReference mUser = mDatabase.getReference().child("KhachHang");

                                    //lưu thông tin khách hàng vào database
                                    mUser.child(userUID).child("email").setValue(email);
                                    mUser.child(userUID).child("password").setValue(password);
                                    mUser.child(userUID).child("tenKH").setValue(ten);
                                    mUser.child(userUID).child("sdt").setValue(sdt);
                                    mUser.child(userUID).child("diaChi").setValue(diaChi);

                                    //lưu ảnh đại diện vào storage và database
                                    storageReference = FirebaseStorage.getInstance().getReference("AnhDaiDien");
                                    if (imageUri != null){
                                        StorageReference picRef = storageReference.child(imageUri.getLastPathSegment());
                                        picRef.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                                Task<Uri> urlTak = taskSnapshot.getStorage().getDownloadUrl();
                                                while (!urlTak.isSuccessful());
                                                Uri downloadUrl = urlTak.getResult();
                                                image_url = downloadUrl.toString();

                                                mUser.child(userUID).child("imageURL").setValue(image_url);
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                            }
                                        });
                                    }

                                    Toast.makeText(getApplicationContext(), "Dang ki thanh cong", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(DangKyKhachHangActivity.this, DangNhapKhachHangActivity.class);
                                    startActivity(intent);
                                }
                                else {
                                    Toast.makeText(getApplicationContext(), "Dang ki that bai, vui long thu lai", Toast.LENGTH_SHORT).show();

                                }
                            }
                        });

            }
        });
    }

    public void nhapLai(){
        btnNhapLai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtEmail.setText("");
                edtPassword.setText("");
                edtRePassword.setText("");
                edtSDT.setText("");
                edtDiaChi.setText("");
                edtEmail.requestFocus();
            }
        });
    }
}
